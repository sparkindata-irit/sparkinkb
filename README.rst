SparkInKB
=========

SparkInKB regroupe plusieurs  base de connaissances utilisé dans le cadre du projet `SparkInData <https://sparkindata.com/>`_.





Dictionnaires des fait climatiques
-------------------------------------

Les dictionnaires des faits climatiques sont repartis sur 3 fichiers :

- `phenomenes-meteo-france.csv <dictionnaires/meteo/phenomenes-meteo-france.csv>`_  : Ce fichier est construit  à partir de la liste des `phénomènes météo <http://www.meteofrance.fr/prevoir-le-temps/phenomenes-meteo>`_ sur  le site de `Météo France <http://www.meteofrance.fr>`_   
- `meteo-mots-faits.csv <dictionnaires/meteo/meteo-mots-faits.csv>`_ : Ce fichier liste un ensemble des mots associés aux faits climatiques extraits en se basant sur les titres des faits marquants dans les `bulletins climatiques <https://donneespubliques.meteofrance.fr/?fond=produit&id_produit=129&id_rubrique=29>`_  mensuels publiés par Météo France. Ces mots permettent de retrouver des expressions, en particulier des groupes nominaux, mentionnant des faits climatiques dans le texte. Par exemple,   l’expression *« vague de chaleur »* est reconnu comme fait climatique  grâce à la présence de mot *chaleur* liste dans ce fichier.
- `meteo-mots.csv  <dictionnaires/meteo/meteo-mots.csv>`_ : Ce fichier contient une liste étendue des mots et des expressions associés aux faits climatiques. Cette liste est aussi extraite des bulletins climatiques de météo France et elle est similaire à la liste précédente sauf qu’elle inclut des adjectifs et des expressions plus longues. L’objectif de cette liste est aussi de reconnaitre des faits climatiques dans le texte.





Fait climatiques marquants en France (2012-2016)
------------------------------------------------


Cette base données a été construite se basant sur  les `bulletins climatiques <https://donneespubliques.meteofrance.fr/?fond=produit&id_produit=129&id_rubrique=29>`_. Elle inclut tout les faits marquants entre janvier 2012 et décembre 2016.  L'extraction des faits est réalisé avec l'outil  `SparkInClimate <https://bitbucket.org/sparkindata-irit/sparkinclimate>`_.  Le fichier `faits-marquants-meteo-france.ndjson <faits/faits-marquants-meteo-france.ndjson>`_  inclut la liste des faits.  Chaque ligne du fichier décrit d’une manière structurée (JSON) un fait.  Le tableau suivant décrit les champs  utilisé pour décrire les faits.


+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Champs                    | Liste | Description                                                                                                                                                                    |
+===========================+=======+================================================================================================================================================================================+
| id                        |       | L'identifiant du fait.                                                                                                                                                         |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| title                     |       | Le titre du fait.                                                                                                                                                              |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| description               |       | La description du fait.                                                                                                                                                        |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| dateIssued                |       | La date de la publication de bulletin climatique.                                                                                                                              |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| startDate                 |       | Date  du fait ou de la début de la période si le fait se prolonge sur une période.                                                                                             |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| endDate                   |       | Date de fin de la période du fait. Corresponds à la date de de début  si le fait ne se prolonge spas sur une période.                                                          |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| date.year                 |       | L'année de la date du fait (ou la date de début de période)                                                                                                                    |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| date.month                |       | Le mois de la date du fait (ou la date de début de période)                                                                                                                    |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| date.day                  |       | Le jour de la date du fait (ou la date de début de période)                                                                                                                    |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| facts                     | √     | Liste de faits                                                                                                                                                                 |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| keywords                  | √     | Mots clées  relatifs aux faits                                                                                                                                                 |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| places                    | √     | Liste des lieux associées aux faits. Cette information est extraite depuis la description des faits dans lequel on mentionne généralement les lieux concernés par les le fait. |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| places.name               |       | Le nom du lieu                                                                                                                                                                 |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| places.country            |       | Le pays du lieu                                                                                                                                                                |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| places.region             |       | Le région du lieu (France)                                                                                                                                                     |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| places.department         |       | Le département du lieu (France)                                                                                                                                                |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| places.prefecture         |       | Le préfecture de département  du lieu (France)                                                                                                                                 |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| places.commune            |       | Le commune du lieu (France)                                                                                                                                                    |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| places.code_insee         |       | Le code INSEE de la commune  (France)                                                                                                                                          |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| places.zip                |       | Le code postal de la commune  (France)                                                                                                                                         |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| places.geometry           |       | La localisation  du lieu. Voir la description de l'objet  Geometry ci-dessous.                                                                                                 |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| region                    |       | La région concerné par le bulletin climatique.                                                                                                                                 |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| region.formatted_address  |       | Le nom de la a région                                                                                                                                                          |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| region.geometry           |       | La localisation  du la resgion. Voir la description de l'objet  Geometry ci-dessous.                                                                                           |
+---------------------------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+


Les différents champs de l'objet *Geometry* utilisé pour décrire les lieux et les régions associées au fait sont listés dans le tableau suivant.

+------------------------+---------------------------------------------------------------------------+
| Champs                 | Description                                                               |
+========================+===========================================================================+
| location               | Localisation de lieu (facultatif). Représenté  par un point sur la carte. |
+------------------------+---------------------------------------------------------------------------+
| location.lat           | Latitude de la localisation                                               |
+------------------------+---------------------------------------------------------------------------+
| location.lon           | Longitude de la localisation                                              |
+------------------------+---------------------------------------------------------------------------+
| viewport               | Zone géographique définit par un rectangle (facultatif).                  |
+------------------------+---------------------------------------------------------------------------+
| viewport.northeast     | Point nord-est du rectangle qui définit la zone géographique              |
+------------------------+---------------------------------------------------------------------------+
| viewport.northeast.lat | Latitude du point nord-est.                                               |
+------------------------+---------------------------------------------------------------------------+
| viewport.northeast.lon | Longitude du point nord-est.                                              |
+------------------------+---------------------------------------------------------------------------+
| viewport.southwest     | Point sud-ouest du rectangle qui définit la zone géographique             |
+------------------------+---------------------------------------------------------------------------+
| viewport.southwest.lat | Latitude du point sud-ouest                                               |
+------------------------+---------------------------------------------------------------------------+
| viewport.southwest.lon | Latitude du point sud-ouest                                               |
+------------------------+---------------------------------------------------------------------------+



Situation sanitaire des vignes (2011-2016)
------------------------------------------------


Cette base données a été construite se basant sur  les `Bulletin de santé du Végétal <http://agriculture.gouv.fr/bulletins-de-sante-du-vegetal>`_  (BSV). Elle inclut toutes les situations  sanitaire  des vignes de la saison 2012 jusqu'à la saison 2016.  Le fichier `situatio-sanitaire-vignes.ndjson <faits/situatio-sanitaire-vignes.ndjson>`_  inclut la liste des situations  observé pour centaines maladies qui concerne les vignes.  Chaque ligne du fichier décrit d’une manière structurée (JSON) une situation sanitaire pour une maladie et une zone agricole.  Le tableau suivant décrit les champs  utilisé pour décrire les situations sanitaire.

+--------------------------+-------+------------------------------------------------------------------------------------------------------------------------------------------------------+
| Champs                   | Liste | Description                                                                                                                                          |
+==========================+=======+======================================================================================================================================================+
| diseases                 | √     | Liste des maladies. Il s'agit généralement d'une seule maladie  mais dans certains cas plusieurs maladies peuvent être reporté dans la même section. |
+--------------------------+-------+------------------------------------------------------------------------------------------------------------------------------------------------------+
| date                     |       | La date du publication de bulletin                                                                                                                   |
+--------------------------+-------+------------------------------------------------------------------------------------------------------------------------------------------------------+
| situation                |       | La situation actuelle de(s) maladies                                                                                                                 |
+--------------------------+-------+------------------------------------------------------------------------------------------------------------------------------------------------------+
| place                    |       | Le lieu géographique concerné par le bulletin climatique.                                                                                            |
+--------------------------+-------+------------------------------------------------------------------------------------------------------------------------------------------------------+
| place.formatted_address  |       |  Le nom du lieu géographique                                                                                                                         |
+--------------------------+-------+------------------------------------------------------------------------------------------------------------------------------------------------------+
| placegeometry            |       |  La localisation du la lieu . Voir la description de l'objet Geometry ci-dessus                                                                      |
+--------------------------+-------+------------------------------------------------------------------------------------------------------------------------------------------------------+



Dictionnaire des maladies de la vigne
-------------------------------------
Le fichier `dictionnaire-maladies-bsv.csv <dictionnaires/agriculture/dictionnaire-maladies-bsv.csv>`_ liste les maladies de la vigne présentes dans les `Bulletin de santé du Végétal <http://agriculture.gouv.fr/bulletins-de-sante-du-vegetal>`_  (BSV).  Le fichier est structuré  en deux colonnes comme présenté dans l’exemple ci-dessous. La première colonne indique les mentions  utilisées  dans le *titres des sections* dans les BSV.  La deuxième colonne permet de mapper ces mentions aux *maladies associées*.

+---------------------+------------------+
| Titre de la section | Maladie          |
+=====================+==================+
| black-dead-arm      | black dead arm   |
+---------------------+------------------+
| black-rot           | black dead arm   |
+---------------------+------------------+
| botrytis            | botrytis         |
+---------------------+------------------+
| ...                 | ...              |
+---------------------+------------------+



Encyclopédie des ravageurs européens (HYPPZ)
--------------------------------------------


La base de données des ravageurs européens (HYPPZ) regroupe 297 ravageurs (insectes, acariens, rongeurs,  etc) en Europe occidentale. Cette base est disponible sur le site `http://www7.inra.fr/hyppz <http://www7.inra.fr/hyppz>`_. Le fichier `hyppz.ndsjon <maladies-ravageurs/hyppz.ndjson>`_  est une image des données disponible sur ce site.  Chaque ligne du fichier décrit d’une manière structurée (JSON) un ravageur.  Le tableau suivant décrit les champs  utilisé pour décrire les ravageurs.


+----------------------------+-------------+-------+-----------------------------------------------------------------------------------------------------------------+
| Champs                     | Multilingue | Liste | Description                                                                                                     |
+============================+=============+=======+=================================================================================================================+
| id                         | √           |       | L’identifiant de ravageur classés par langue.. Cette information est extraite depuis l'URL de la page.          |
+----------------------------+-------------+-------+-----------------------------------------------------------------------------------------------------------------+
| url                        | √           |       | URLs associées au ravageur classées par langue. Cette information est extraite depuis l'URL de la page.         |
+----------------------------+-------------+-------+-----------------------------------------------------------------------------------------------------------------+
| scientific_names           |             | √     | La classification scientifiques du ravageur.. Cette information est extraite depuis l'entête de la page.        |
+----------------------------+-------------+-------+-----------------------------------------------------------------------------------------------------------------+
| scientific_classification  |             | √     | Les noms scientifiques du ravageur. Cette information est extraite depuis l'entête de la page.                  |
+----------------------------+-------------+-------+-----------------------------------------------------------------------------------------------------------------+
| usage_names                | √           | √     | Les noms d'usage du ravageur. Cette information est extraite depuis l'entête de la page.                        |
+----------------------------+-------------+-------+-----------------------------------------------------------------------------------------------------------------+
| description                | √           |       | La description du ravageur. Cette information est extraite depuis la section *Description*.                     |
+----------------------------+-------------+-------+-----------------------------------------------------------------------------------------------------------------+
| biology                    | √           |       | Détails biologiques  sur le ravageur. Cette information est extraite depuis la section *Biologie*.              |
+----------------------------+-------------+-------+-----------------------------------------------------------------------------------------------------------------+
| life_cycle                 | √           |       | Les cycles de vie du ravageur. Cette information est extraite depuis la section *Cycle de vie*.                 |
+----------------------------+-------------+-------+-----------------------------------------------------------------------------------------------------------------+
| damage                     | √           |       | Le dégâts causé par le ravageur . Cette information est extraite depuis la section *Dégâts*.                    |
+----------------------------+-------------+-------+-----------------------------------------------------------------------------------------------------------------+
| common_names               | √           |       | Les différents noms commun du ravageur. Cette information est extraite depuis la section *Noms communs*.        |
+----------------------------+-------------+-------+-----------------------------------------------------------------------------------------------------------------+
| images.                    |             | √     | Liste des images. Cette information est extraite depuis la section *Images*.                                    |
+----------------------------+-------------+-------+-----------------------------------------------------------------------------------------------------------------+
| images.description         |             |       | La description de l'image. Cette information est extraite depuis la section *Images*.                           |
+----------------------------+-------------+-------+-----------------------------------------------------------------------------------------------------------------+
| images.scientific_name     |             |       | Le nom scientifique utilisé comme lien vers l'image. Cette information est extraite depuis la section *Images*. |
+----------------------------+-------------+-------+-----------------------------------------------------------------------------------------------------------------+
| images.url                 |             |       | L'URL de l'image. Cette information est extraite depuis la section *Images*.                                    |
+----------------------------+-------------+-------+-----------------------------------------------------------------------------------------------------------------+




Base des maladies, ravageurs et adventices Bayer-Agri
------------------------------------------------------

La base  Bayer-Agri inclut plus de 200 maladies, ravageurs et adventices répertoriés sur 28 cultures.  Cette base est disponible sur le site Web `http://www.bayer-agri.fr/  <http://www.bayer-agri.fr/>`_.   Le fichier `bayer-agri.ndsjon <maladies-ravageurs/bayer-agri.ndjson>`_  est une image des données disponible sur ce site.  Chaque ligne du fichier décrit d’une manière structurée (JSON) une anomalie.  Le tableau suivant décrit les champs  utilisé pour décrire les ravageurs.


+-----------------------------+-------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Champs                      | Liste | Description                                                                                                                                                                                                                                               |
+=============================+=======+===========================================================================================================================================================================================================================================================+
| url                         |       | L'URL de la page de l'anomalie                                                                                                                                                                                                                            |
+-----------------------------+-------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| names                       | √     | Les noms  de l'anomalie                                                                                                                                                                                                                                   |
+-----------------------------+-------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| family                      |       | La famille de l'anomalie:  maladie, ravageur ou adventice                                                                                                                                                                                                 |
+-----------------------------+-------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| description                 |       | La  description de l'anomalie                                                                                                                                                                                                                             |
+-----------------------------+-------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| characteristics             | √     | Liste des caractéristique  qui represente  le profile l'anomalie                                                                                                                                                                                          |
+-----------------------------+-------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| characteristics.title       |       | Le titre de la caractéristique de l'anomalie.  Parmi ces caractéristique on trouve : symptômes,  nuisibilité, cycle,. Une liste complète des valeurs possibles est présente dans ce `fichier <maladies-ravageurs/bayer-agri-caractecteristique.csv>`_     |
+-----------------------------+-------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| characteristics.description |       | La description de la caractéristique.                                                                                                                                                                                                                     |
+-----------------------------+-------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| images                      | √     | Liste des images                                                                                                                                                                                                                                          |
+-----------------------------+-------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+



Version
===============

SparkInKB  1.0.0


Collaborateurs
===============

Les personnes suivantes ont contribué à la collecte de ces données

- Lamjed Ben Jabeur `Lamjed.Ben-Jabeur@irit.fr <mailto:Lamjed.Ben-Jabeur@irit.fr>`_.


License
===============
This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software.  You can  use, modify and/ or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL
`http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html <http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html>`_.